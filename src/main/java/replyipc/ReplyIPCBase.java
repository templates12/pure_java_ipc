package replyipc;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.HashMap;

import org.stringtree.json.ExceptionErrorListener;
import org.stringtree.json.JSONReader;
import org.stringtree.json.JSONValidatingReader;
import org.stringtree.json.JSONValidatingWriter;
import org.stringtree.json.JSONWriter;

import com.aicas.jamaica.car.system.logging.LogLevel;
import com.aicas.jamaica.car.system.logging.Logger;

import carlogger.LoggerManager;


public class ReplyIPCBase {

	static {
		LoggerManager.setUseRealLogger(false);
	}
	static Logger log = LoggerManager.getLogger(ReplyIPCBase.class);

	protected final String CMD = "cmd";
	protected final String TOPIC = "topic";
	protected final String CLIENTID = "clientID";
	protected final String REPLY = "reply";
	protected final String DATA = "data";
	protected final String REGISTER_PRODUCER = "register_producer";
	protected final String REGISTER_CONSUMER =  "register_consumer";
	protected final String UNREGISTER_PRODUCER = "unregister_producer";
	protected final String UNREGISTER_CONSUMER =  "unregister_consumer";
	protected final String PRODUCE =  "produce";
	protected final String COMSUME =  "consume";

	static final int  serverPort = 10101;
    static final String serverHost  = "localhost";
    static final String serverTopic  = "MAIN_TOPIC";
    protected String brokerHost;
    protected  int  brokerPort;
    protected String producerTopic;
    public String clientID;

    /**
     * constructor
     * @param  brokerHost      broker message server host
     * @param  brokerPort      broker message server port
     * @param  producerTopic   topic name
     */
    public ReplyIPCBase(String brokerHost, Integer brokerPort, String producerTopic){
        if (!brokerHost.isEmpty())
            this.brokerHost = brokerHost;
        else
            this.brokerHost = serverHost;

        if (brokerPort != 0)
            this.brokerPort = brokerPort;
        else
            this.brokerPort = serverPort;

        if (!producerTopic.isEmpty())
            this.producerTopic = producerTopic;
        else
            this.producerTopic = serverTopic;
    }
    
    /**
     * send a message to broker server
     * @param  brokerHost      broker message server host
     * @param  brokerPort      broker message server port
     * @param  producerTopic   topic name
     * @return String  json result from server side
     */
    protected String send2broker(String brokerHost, Integer brokerPort, String msg){
        Socket socket = null;
        String result = null;
        try{
            try {
                InetAddress ipAddress = InetAddress.getByName(brokerHost);
                socket = new Socket(ipAddress, brokerPort);
                InputStream  sin  = socket.getInputStream();
                OutputStream sout = socket.getOutputStream();
                DataInputStream  in  = new DataInputStream (sin );
                DataOutputStream out = new DataOutputStream(sout);
                out.writeUTF(msg);
                out.flush();
                log.log(LogLevel.INFO, "ReplyIPCBase: send2broker: send - " + msg);
                result = in.readUTF();
                log.log(LogLevel.INFO, "ReplyIPCBase: send2broker: receive - " + result);
            }
            catch (Exception e) {
                // e.printStackTrace();
                return result;
            }
        }
        finally {
            try {
                if (socket != null)
                    socket.close();
            }
            catch (IOException e) {
                // e.printStackTrace();
                return result;
            }
        }
        return result;
    }

    /**
     * jsonORM - convert json string to java HashMap object 
     * @param  input   json string
     * @return java object
     */
    @SuppressWarnings("unchecked")
	protected HashMap<String, Object> json2map(final String input) {
        JSONReader reader = new JSONValidatingReader(new ExceptionErrorListener());
        HashMap<String, Object> obj = new HashMap<String, Object>();
        try {
            obj = (HashMap<String, Object>) reader.read(input);
        }
        catch(IllegalArgumentException e) {
            return null;
        }
        return obj;
    }

    /**
     * jsonORM - convert from java HashMap object to json string 
     * @param  obj   java object
     * @return json string
     */
    protected String map2json(final HashMap<String, Object> obj) {
        JSONWriter writer = new JSONValidatingWriter(new ExceptionErrorListener());
        Object dd = null;
        try {
            dd = writer.write(obj);
        }
        catch(IllegalArgumentException e) {
            return null;
        }           
        return dd.toString();
    }

    /**
     * do registration or unregistration of producer or consumer AND set clientID param
     * @param  brokerHost      broker message server host
     * @param  brokerPort      broker message server port
     * @param  topic           topic name
     * @param  producer        true if producer shall be (un)register
     * @param  register        true if it registration cmd
     * @return true or false
     */
    public boolean registerOrleave(String brokerHost, Integer brokerPort, String topic, 
                                   boolean producer, boolean register) {
        if (brokerHost.isEmpty())
            brokerHost = this.brokerHost;
        if (brokerPort.equals(0))
            brokerPort = this.brokerPort;
        if (topic.isEmpty())
            topic = this.producerTopic;

        HashMap<String, Object> jreq = new HashMap<String, Object>();
        HashMap<String, Object> jres = new HashMap<String, Object>();
        
        String jmsg = null;
        String jreply = null;
        if (register) {
            if (producer)
                jreq.put(CMD, REGISTER_PRODUCER);
            else
                jreq.put(CMD, REGISTER_CONSUMER);
            jreq.put(TOPIC, topic);
        }
        else {
            if (producer)
                jreq.put(CMD, UNREGISTER_PRODUCER);
            else
                jreq.put(CMD, UNREGISTER_CONSUMER);
            jreq.put(CLIENTID, this.clientID);
        }
        
        jmsg = map2json(jreq);
        if (jmsg == null)
            return false;
        
        jreply = send2broker(brokerHost, brokerPort, jmsg);
        if (jreply == null)
            return false;
        
        jres = json2map(jreply);
        if (jres == null)
            return false;
 
        Long return_code = (Long) jres.get(REPLY);
        if (return_code.intValue() != 200)
            return false;

        String client = (String) jres.get(CLIENTID);
        if (client == null)
            return false;

        this.clientID = client;
        return true;
    }
}