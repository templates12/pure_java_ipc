/**
    Reply. 2018
    IPC Broker
 *
*/

package replyipc;

import java.util.HashMap;
import java.util.UUID;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.stringtree.json.ExceptionErrorListener;
import org.stringtree.json.JSONReader;
import org.stringtree.json.JSONValidatingReader;
import org.stringtree.json.JSONValidatingWriter;
import org.stringtree.json.JSONWriter;

import com.aicas.jamaica.car.system.logging.LogLevel;
import com.aicas.jamaica.car.system.logging.Logger;

import carlogger.LoggerManager;


public class ReplyIPCServer implements Runnable {
    private int serverPort;
	private final ServerSocket serverSocket;
    private final ExecutorService pool;
    private final int maxConsumerQueueSize = 50;
    private final int maxConsumerSize = 100;
    private final int maxExecutorPoolSize = 50;
    private final int maxTopicCount = 100;
    private final long maxConsumMilisecondTimeout = 1000*60; //1 minute
    private final int stratagyKeepQueueSize = 10;
    private final String DISCARD_STRATEGY = "DontKeepIfNoSubscribes";
    private final String RETAIN_STRATEGY = "KeepSeveralUntilConsumerSubscribed";
    private String stratagy = RETAIN_STRATEGY;
    
    private final String CMD = "cmd";
    private final String REQUEST = "request";
    private final String TOPIC = "topic";
    private final String CLIENTID = "clientID";
    private final String REPLY = "reply";
    private final String ERROR = "error";
    private final String DATA = "data";
    private final String REMAIN = "remain";
    private final String REGISTER_PRODUCER = "register_producer";
    private final String REGISTER_CONSUMER =  "register_consumer";
    private final String UNREGISTER_PRODUCER = "unregister_producer";
    private final String UNREGISTER_CONSUMER =  "unregister_consumer";
    private final String PRODUCE =  "produce";
    private final String COMSUME =  "consume";

    private ConcurrentHashMap<String, String> producerTopic = new ConcurrentHashMap<String, String>(maxExecutorPoolSize*maxTopicCount);
    private ConcurrentHashMap<String, String> consumerTopic = new ConcurrentHashMap<String, String>(maxExecutorPoolSize*maxTopicCount);
    private ConcurrentHashMap<String, CopyOnWriteArrayList<String>> topicProducers = new ConcurrentHashMap<String, CopyOnWriteArrayList<String>>(maxTopicCount);
    private ConcurrentHashMap<String, CopyOnWriteArrayList<String>> topicConsumers = new ConcurrentHashMap<String, CopyOnWriteArrayList<String>>(maxTopicCount);
    private ConcurrentHashMap<String, ConcurrentHashMap<String, BlockingQueue<String>>> topicConsumersQueues = new ConcurrentHashMap<String, ConcurrentHashMap<String, BlockingQueue<String>>>(maxTopicCount);    
    private ConcurrentHashMap<String, BlockingQueue<String>> topicQueues = new ConcurrentHashMap<String, BlockingQueue<String>>(maxTopicCount);

    public boolean stopRequired = false;

	static {
		LoggerManager.setUseRealLogger(false);
	}
	static Logger log = LoggerManager.getLogger(ReplyIPCServer.class);

    /**
     * This class handle any connections from client
     * @param  socket client socket
     * @throws IOException
     */
    class Handler implements Runnable {
        private final Socket socket;
        Handler(Socket socket) { this.socket = socket; }
        public void run() {
           // read and service request on socket
            InputStream sin;
            OutputStream sout;
            DataInputStream  dis;
            DataOutputStream dos;
            String line = null;
            try {
                sin = socket.getInputStream();
                sout = socket.getOutputStream();
                dis = new DataInputStream (sin );
                dos = new DataOutputStream(sout);
                line = dis.readUTF();
                log.log(LogLevel.INFO, "ReplyIPCServer: Handler: get - " + line);
                String res = runCMD(line);
                dos.writeUTF(res);
                log.log(LogLevel.INFO, "ReplyIPCServer: Handler: sent - " + res);
                dos.flush();
            } catch (IOException e) {
            	// seems connection was closed by pear. Do nothing
            	log.log(LogLevel.WARN, "ReplyIPCServer: Handler: IOException", e);
            }
        }
    }

    /**
     * Constructor
     * @param  port number
     * @param  poolSize count of executors
     * @param  stratagy a couple strategies supported by server: DontKeepIfNoSubscribes and SeveralKeepUntilConsumerSubscribed.
     * 			SeveralKeepUntilConsumerSubscribed by default
     * @throws IOException  due to executor 
     */
    public ReplyIPCServer(int port, int poolSize, String stratagy) throws IOException {
        if (poolSize == 0)
            poolSize = this.maxExecutorPoolSize;
        else if (!(poolSize > 0 && poolSize < 51))
            poolSize = this.maxExecutorPoolSize;
        if (!stratagy.isEmpty() &&
            (stratagy.equals(DISCARD_STRATEGY) ||
             stratagy.equals(RETAIN_STRATEGY)))
            this.stratagy = stratagy;
        else
            this.stratagy = RETAIN_STRATEGY;
        this.serverPort = port;
        serverSocket = new ServerSocket(port);
        pool = Executors.newFixedThreadPool(poolSize);
    }

    /**
     * Return port number which server is occupied
     * @return server port number
     */
    public int getPort() {
        return this.serverPort;
    }

    /**
     * ORM from String json to corresponding Java Object
     * @param  input   json string
     * @return HashMap java object
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
	private HashMap<String, Object> json2map(final String input) {
        JSONReader reader = new JSONValidatingReader(new ExceptionErrorListener());
        HashMap<String, Object> obj = new HashMap<String, Object>();
        try {
            obj = (HashMap<String, Object>) reader.read(input);
        }
        catch(IllegalArgumentException e) {
        	log.log(LogLevel.WARN, "ReplyIPCServer: json2map: IllegalArgumentException", e);
        	return null;
        }
        return obj;
    }

    /**
     * ORM from Java HashMap to json string
     * @param  obj   HashMap
     * @return java string
     */
    private String map2json(final HashMap<String, Object> obj) {
        JSONWriter writer = new JSONValidatingWriter(new ExceptionErrorListener());
        Object dd = null;
        try {
            dd = writer.write(obj);
        }
        catch(IllegalArgumentException e) {
        	log.log(LogLevel.WARN, "ReplyIPCServer: map2json: IllegalArgumentException", e);
        	return null;
        }
        return dd.toString();
    }

    /**
     * check json protocol before to handle it
     * @param  obj   HashMap
     * @return java string
     */
    private String commonChecks(String data) {
        HashMap<String, Object> jres = new HashMap<String, Object>();
        HashMap<String, Object> jreply = new HashMap<String, Object>();

        log.log(LogLevel.INFO, "ReplyIPCServer: commonChecks: data - " + data);

        jres = json2map(data);
        if (jres == null) {
            jreply.put(REQUEST, "None");
            jreply.put(TOPIC, "None");
            jreply.put(REPLY, 500);
            jreply.put(ERROR, "Json Reader issue");
            return map2json(jreply);
        }

        String cmd = (String) jres.get(CMD);
        String topic = (String) jres.get(TOPIC);
        String clientid = null;

        if (cmd == null) {
            jreply.put(REQUEST, "None");
            jreply.put(TOPIC, topic);
            jreply.put(REPLY, 401);
            jreply.put(ERROR, "No command");
            return map2json(jreply);
        }
        else if(!(cmd.equals(REGISTER_PRODUCER) ||
        		 cmd.equals(REGISTER_CONSUMER) ||
        		 cmd.equals(UNREGISTER_PRODUCER) ||
        		 cmd.equals(UNREGISTER_CONSUMER) ||
        		 cmd.equals(PRODUCE) ||
        		 cmd.equals(COMSUME))) {
            jreply.put(REQUEST, cmd);
            jreply.put(TOPIC, topic);
            jreply.put(REPLY, 400);
            jreply.put(ERROR, "Command is not supported");
            return map2json(jreply);
        }
        jreply.put(REQUEST, jres.get(CMD));

        if (cmd.equals(UNREGISTER_PRODUCER) ||
            cmd.equals(UNREGISTER_CONSUMER) ||
            cmd.equals(PRODUCE) ||
            cmd.equals(COMSUME)) {

            // if no cleintID then no chance to find topic per clintID
        	// let's say goodbye
        	clientid = (String) jres.get(CLIENTID);
            if (clientid == null || clientid.isEmpty()) {
                jreply.put(TOPIC, topic);
                jreply.put(REPLY, 403);
                jreply.put(ERROR, "No client ID");
                return map2json(jreply);
            }
            if (cmd.equals(PRODUCE)) {
                String d = (String) jres.get(DATA);
                if (d == null) {
                    jreply.put(TOPIC, topic);
                    jreply.put(REPLY, 404);
                    jreply.put(ERROR, "No data");
                    return map2json(jreply);
                }
            }

            if (cmd.equals(UNREGISTER_PRODUCER) || cmd.equals(PRODUCE)) {
                // let's find registered topic or return error back
            	// because topic must be created during registration
            	topic = producerTopic.get(clientid);
                if (topic == null) {
                    jreply.put(TOPIC, topic);
                    jreply.put(CLIENTID, clientid);
                    jreply.put(REPLY, 402);
                    jreply.put(ERROR, "No Topic in request");
                    return map2json(jreply);
                }
                if (!topicProducers.containsKey(topic)) {
                    jreply.put(TOPIC, topic);
                    jreply.put(CLIENTID, clientid);
                    jreply.put(REPLY, 406);
                    jreply.put(ERROR, "Have No Topic registered in system");
                    return map2json(jreply);
                }
            }
            else if (cmd.equals(UNREGISTER_CONSUMER) || cmd.equals(COMSUME)) {
                // let's find registered topic or return error back
            	// because topic must be created during registration
            	topic = consumerTopic.get(clientid);
                if (topic == null) {
                    jreply.put(TOPIC, topic);
                    jreply.put(CLIENTID, clientid);
                    jreply.put(REPLY, 402);
                    jreply.put(ERROR, "No Topic in request");
                    return map2json(jreply);
                }
                if (!topicConsumers.containsKey(topic)) {
                    jreply.put(TOPIC, topic);
                    jreply.put(CLIENTID, clientid);
                    jreply.put(REPLY, 406);
                    jreply.put(ERROR, "Have No Topic registered in system");
                    return map2json(jreply);
                }
            }
        }
        // if no topic was mentioned then it useless for future
        // let's say goodbye
        if (cmd.equals(REGISTER_PRODUCER)) {
            if (topic == null) {
                jreply.put(TOPIC, topic);
                jreply.put(REPLY, 402);
                jreply.put(ERROR, "No Topic in request");
                return map2json(jreply);
            }
            if(this.stratagy.equals(this.DISCARD_STRATEGY) && 
               (!topicConsumers.containsKey(topic) ||
                !topicConsumersQueues.containsKey(topic))){
                jreply.put(TOPIC, topic);
                jreply.put(REPLY, 408);
                jreply.put(ERROR, "No Consumers. No reason to keep data");
                return map2json(jreply);
            }
        }
        if (cmd.equals(REGISTER_CONSUMER)) {
            if (topic == null) {
                jreply.put(TOPIC, topic);
                jreply.put(REPLY, 402);
                jreply.put(ERROR, "No Topic in request");
                return map2json(jreply);
            }
            if (this.stratagy.equals(this.DISCARD_STRATEGY) && !topicProducers.containsKey(topic)) {
                jreply.put(TOPIC, topic);
                jreply.put(REPLY, 407);
                jreply.put(ERROR, "No Topic for Consumer was created");
                return map2json(jreply);
            }
        }
        return null;
    }

    /**
     * run command
     * @param  data  json string
     * @return json string
     */
    private String runCMD(String data) {
        HashMap<String, Object> jres = new HashMap<String, Object>();
        HashMap<String, Object> jreply = new HashMap<String, Object>();
        String check = commonChecks(data);
        // if checks were failed no reason to continue. go away
        if (check != null)
            return check;

        log.log(LogLevel.INFO, "ReplyIPCServer: runCMD: data - " + data);

        jres = json2map(data);
        String cmd = (String) jres.get(CMD);
        jreply.put(REQUEST, cmd);
 
        // let's find what command is and run it
        if (cmd.equals(REGISTER_PRODUCER)) {
            String topic = (String) jres.get(TOPIC);
            jreply.put(TOPIC, topic);
            registerProducer(jres, jreply);
        }
        else if (cmd.equals(REGISTER_CONSUMER)) {
            String topic = (String) jres.get(TOPIC);
            jreply.put(TOPIC, topic);
            registerConsumer(jres, jreply);
        }
        else if (cmd.equals(UNREGISTER_PRODUCER)) {
            String clientUUID = (String) jres.get(CLIENTID);
            String topic = producerTopic.get(clientUUID);;
              jreply.put(TOPIC, topic);
            jreply.put(CLIENTID, clientUUID);
            unregisterProducer(jres, jreply);
        }
        else if (cmd.equals(UNREGISTER_CONSUMER)) {
            String clientUUID = (String) jres.get(CLIENTID);
            String topic = consumerTopic.get(clientUUID);
            jreply.put(TOPIC, topic);
            jreply.put(CLIENTID, clientUUID);
            unregisterConsumer(jres, jreply);
        }
        else if (cmd.equals(PRODUCE)) {
            String clientUUID = (String) jres.get(CLIENTID);
            String topic = producerTopic.get(clientUUID);
            jreply.put(TOPIC, topic);
            jreply.put(CLIENTID, clientUUID);
            produce(jres, jreply);
        }
        else if (cmd.equals(COMSUME)) {
            String clientUUID = (String) jres.get(CLIENTID);
            String topic = consumerTopic.get(clientUUID);
            jreply.put(TOPIC, topic);
            jreply.put(CLIENTID, clientUUID);
            consume(jres, jreply);
        }
        return map2json(jreply);
    }

    /**
     * create a topic and fill corresponding java objects 
     * @param  topic  topic name
     * @return true or false
     */
    private boolean createTopic(String topic) {
        log.log(LogLevel.INFO, "ReplyIPCServer: createTopic: topic - " + topic);

    	if (topic == null || topic.isEmpty())
    		return false;

        // Let's check that we don't have such topic and create it
        if (!topicProducers.containsKey(topic)) {
            CopyOnWriteArrayList<String> value = new CopyOnWriteArrayList<String>();
            topicProducers.put(topic, value);
        }
        if (this.stratagy.equals(RETAIN_STRATEGY)) {
            if (!topicQueues.containsKey(topic)) {
                BlockingQueue<String> d = new ArrayBlockingQueue<String>(stratagyKeepQueueSize);
                topicQueues.put(topic, d);
            }
        }    		
		if (!topicConsumers.containsKey(topic)) {
            CopyOnWriteArrayList<String> value = new CopyOnWriteArrayList<String>();
            topicConsumers.put(topic, value);
        }
        if (!topicConsumersQueues.containsKey(topic)) {
            ConcurrentHashMap<String, BlockingQueue<String>> consumerQueue = new ConcurrentHashMap<String, BlockingQueue<String>>(maxConsumerSize);
            topicConsumersQueues.put(topic, consumerQueue);
        }	
    	return true;
    }

    /**
     * register producer for specific topic mentioned by producer.
     * Supports only: one producer has only one topic.
     * @param  jres  request Java object which come from client
     * @param  jreply  response Java object filled by server
     * @return void
     */
    private void registerProducer(HashMap<String, Object> jres, HashMap<String, Object> jreply) {
        String producerUUID = UUID.randomUUID().toString();
        String topic = (String) jres.get(TOPIC);

        log.log(LogLevel.INFO, "ReplyIPCServer: registerProducer: topic - " + topic);

        createTopic(topic);        
        topicProducers.get(topic).add(producerUUID);
        producerTopic.put(producerUUID, topic);

        jreply.put(CLIENTID, producerUUID);
        jreply.put(REPLY, 200);
    }

    /**
     * unregister producer for specific topic mentioned by producer.
     * if no producers any more then topic will be deleted from topicQueues
     * @param  jres  request Java object which come from client
     * @param  jreply  response Java object filled by server
     * @return void
     */
    private void unregisterProducer(HashMap<String, Object> jres, HashMap<String, Object> jreply) {
        String clientUUID = (String) jres.get(CLIENTID);
        String topic = producerTopic.get(clientUUID);

        log.log(LogLevel.INFO, "ReplyIPCServer: unregisterProducer: topic - " + topic + "; client: " + clientUUID);

        if (producerTopic != null && !producerTopic.isEmpty())
            producerTopic.remove(clientUUID);

        // we are going to remove topic and
        // we shall guarantee that no producers were registered under this queue
        // before to remove topic from topicQueues
        synchronized (this){
            if (topicProducers != null && !topicProducers.isEmpty()) {
                CopyOnWriteArrayList<String> producers  = topicProducers.get(topic);
                // let's remove producer per this topic
                if (producers != null && !producers.isEmpty()) 
                    producers.remove(clientUUID);
                // TODO: do we need to clean queue per topic dispute consumers have not come yet?
                // Let's clear queue if no producers registered for this topic
                if (this.stratagy.equals(RETAIN_STRATEGY)) {
                    if (producers!= null && producers.isEmpty() && topicQueues != null && !topicQueues.isEmpty())
                        topicQueues.remove(topic);
                }
            }
            this.notify();
        }
 
        jreply.put(REPLY, 200);
    }

    /**
     * put data to specific topic and spread it through all subscribers
     * @param  jres  request Java object which come from client
     * @param  jreply  response Java object filled by server
     * @return void
     */
    private void produce(HashMap<String, Object> jres, HashMap<String, Object> jreply) {
        String clientUUID = (String) jres.get(CLIENTID);
        String data = (String) jres.get(DATA);
        String topic = producerTopic.get(clientUUID);

        log.log(LogLevel.INFO, "ReplyIPCServer: produce: topic - " + topic + "; client: " + clientUUID);

        try {
            ConcurrentHashMap<String, BlockingQueue<String>> consumerQueue = topicConsumersQueues.get(topic);
            if (consumerQueue != null) {
                for(String i: consumerQueue.keySet()){
                    BlockingQueue<String> d = consumerQueue.get(i);
                    boolean res = d.offer(data, 100, TimeUnit.MILLISECONDS);
                    if (!res) {
                        jreply.put(REPLY, 501);
                        jreply.put(ERROR, "No Space per topic");
                        return;
                    }
                }
            }

            /* seems we dont have registered consumers.
             * Let's keep messages in separately queue
             * and remove older and put new one*/
            if (this.stratagy.equals(RETAIN_STRATEGY) && !topicConsumers.containsKey(topic)) {
                // we shall synchronize it because we shall guarantee 2 operation - remove older and put new one.  
            	synchronized (this){
                    if (topicQueues != null) {
                        BlockingQueue<String> d = topicQueues.get(topic);
                        if (d != null) {    
                            try {
                                //d.isEmpty() || d.size() <= stratagyKeepQueueSize
                                d.add(data);
                            }
                            // if not capacity then let's to remove older one
                            catch (IllegalStateException e)
                            {
                            	d.poll();
                                d.put(data);
                            }
                        }
                    } this.notify();
                }
            }
        }
        catch (InterruptedException e) {
        	log.log(LogLevel.WARN, "ReplyIPCServer: produce: InterruptedException", e);
        	jreply.put(REPLY, 500);
            jreply.put(ERROR, "Server issue on PUT");
            return;
        }
        jreply.put(REPLY, 200);
    }

    /**
     * register consumer and create a topic if is required
     * @param  jres  request Java object which come from client
     * @param  jreply  response Java object filled by server
     * @return void
     */
    private void registerConsumer(HashMap<String, Object> jres, HashMap<String, Object> jreply) {
        String consumerUUID = UUID.randomUUID().toString();
        String topic = (String) jres.get(TOPIC);
        int remain = 0;

        log.log(LogLevel.INFO, "ReplyIPCServer: registerConsumer: topic - " + topic);

        createTopic(topic);
        consumerTopic.put(consumerUUID, topic);
        topicConsumers.get(topic).add(consumerUUID);

        ConcurrentHashMap<String, BlockingQueue<String>> consumerQueue = topicConsumersQueues.get(topic);
        if(consumerQueue.isEmpty() || !consumerQueue.containsKey(consumerUUID)) {
            BlockingQueue<String> d = new ArrayBlockingQueue<String>(maxConsumerQueueSize);
            consumerQueue.put(consumerUUID, d);
            topicConsumersQueues.put(topic, consumerQueue);
        }

        if (this.stratagy.equals(RETAIN_STRATEGY) && topicConsumers.containsKey(topic)) {
            // we have to guarantee that transmission from topicQueues to consumer queue
        	// will not be interrupted by other thread
        	synchronized (this){
                // Let's do a copy of all messages from TopicQueue to first consumer queue
                if (topicQueues != null && !topicQueues.isEmpty() && topicQueues.containsKey(topic)) {
                    BlockingQueue<String> d = topicQueues.get(topic);
                    ConcurrentHashMap<String, BlockingQueue<String>> consumerQueue2 = topicConsumersQueues.get(topic);
                    if (consumerQueue2 != null) {
                        BlockingQueue<String> d2 = consumerQueue2.get(consumerUUID);
                        if(d2 != null && d2.isEmpty() && d != null && !d.isEmpty()){
                            d2.addAll(d);
                            d.clear();
                        }
                        remain = d2.size();
                        // no reason to keep this topic anymore
                        topicQueues.remove(topic);
                    }
                }
                this.notify();
            }
        }

        jreply.put(CLIENTID, consumerUUID);
        jreply.put(REMAIN, remain);
        jreply.put(REPLY, 200);
    }

    /**
     * unregister consumer
     * @param  jres  request Java object which come from client
     * @param  jreply  response Java object filled by server
     * @return void
     */
    private void unregisterConsumer(HashMap<String, Object> jres, HashMap<String, Object> jreply) {
        String clientUUID = (String) jres.get(CLIENTID);
        String topic = consumerTopic.get(clientUUID);

        log.log(LogLevel.INFO, "ReplyIPCServer: unregisterConsumer: topic - " + topic + "; client: " + clientUUID);

        if (consumerTopic != null && !consumerTopic.isEmpty())
            consumerTopic.remove(clientUUID);

        CopyOnWriteArrayList<String> consumers  = topicConsumers.get(topic);
        if (consumers != null && !consumers.isEmpty())
            consumers.remove(clientUUID);

        ConcurrentHashMap<String, BlockingQueue<String>> consumerQueue = topicConsumersQueues.get(topic);
        if (consumerQueue != null && !consumerQueue.isEmpty())
            consumerQueue.remove(clientUUID);

        jreply.put(REPLY, 200);
    }

    /**
     * consume data from queue belong this consumers and related to specific topic
     * @param  jres  request Java object which come from client
     * @param  jreply  response Java object filled by server
     * @return void
     */
    private void consume(HashMap<String, Object> jres, HashMap<String, Object> jreply) {
        String clientUUID = (String) jres.get(CLIENTID);
        long waitUntil = (null == jres.get("waitUntil")) ? (Long)maxConsumMilisecondTimeout : (Long)jres.get("waitUntil");
        boolean waitFlag = (null == jres.get("waitUntil")) ? false:true;
        if (waitFlag && (waitUntil < 0 || waitUntil > maxConsumMilisecondTimeout))
            waitUntil = maxConsumMilisecondTimeout;
        String topic = consumerTopic.get(clientUUID);
        String data = null;
        int remain = 0;

        log.log(LogLevel.INFO, "ReplyIPCServer: consume: topic - " + topic + "; client: " + clientUUID);

        try {
            if (topicConsumersQueues != null && !topicConsumersQueues.isEmpty()) {
                ConcurrentHashMap<String, BlockingQueue<String>> consumerQueue = topicConsumersQueues.get(topic);
                if (consumerQueue != null) {
                    BlockingQueue<String> d = consumerQueue.get(clientUUID);
                    if (waitFlag && d!= null) {
                        data = d.poll(waitUntil, TimeUnit.MILLISECONDS);
                        remain = d.size();
                        if (data == null) {
                            jreply.put(REPLY, 444);
                            jreply.put(ERROR, "No data");
                            return;
                        }
                    }
                    else if (d != null && !d.isEmpty()){
                        data = d.poll();
                        remain = d.size();
                        if (data == null) {
                            jreply.put(REPLY, 444);
                            jreply.put(ERROR, "No data");
                            return;
                        }
                    }
                    else {
                        jreply.put(REPLY, 444);
                        jreply.put(ERROR, "No data");
                        return;
                    }
                }
                else {
                    jreply.put(REPLY, 445);
                    jreply.put(ERROR, "No data for requested topic");
                    return;
                }
            }
            else {
                jreply.put(REPLY, 445);
                jreply.put(ERROR, "No data for requested topic");
                return;
            }
        }
        catch (InterruptedException e) {
        	log.log(LogLevel.WARN, "ReplyIPCServer: consume: InterruptedException", e);
        	jreply.put(REPLY, 500);
            jreply.put(ERROR, "Server issue on pool data");
            return;
        }

        if (waitFlag)
            jreply.put("waitUntil", waitUntil);
        jreply.put(DATA, data.toString());
        jreply.put(REMAIN, remain);
        jreply.put(REPLY, 200);
    }

    /**
     * run the server
     */
    public void run() { // run the service
    	log.log(LogLevel.INFO, "ReplyIPCServer: run");

    	try {
            while(!stopRequired) {
                pool.execute(new Handler(serverSocket.accept()));
            }
        }
        catch (IOException e) {
        	log.log(LogLevel.WARN, "ReplyIPCServer: run: IOException", e);
        	pool.shutdown();
        }
    }
}