package replyipc;

import java.util.HashMap;
import com.aicas.jamaica.car.system.logging.LogLevel;


public class ReplyIPCProducer extends ReplyIPCBase {

	/**
     * Constructor
     * @param  brokerHost      broker message server host
     * @param  brokerPort      broker message server port
     * @param  producerTopic   topic name
     */
	public ReplyIPCProducer(String brokerHost, Integer brokerPort, String producerTopic) {
		super(brokerHost, brokerPort, producerTopic);
	}

    /**
     * do a registration
     * @return true or false
     */
	public boolean register() {
		log.log(LogLevel.INFO, "ReplyIPCProducer: register");
		return registerOrleave(brokerHost, brokerPort, producerTopic, true, true);
    }

    /**
     * do a unregistration
     * @return true or false
     */
    public boolean leave() {
		log.log(LogLevel.INFO, "ReplyIPCProducer: leave");
    	return registerOrleave(brokerHost, brokerPort, producerTopic, true, false);
    }

    /**
     * produce
     * @param  data   string data which shall be put to topic
     * @return true or false
     */
    public boolean produce(String data) {
    	HashMap<String, Object> jreq = new HashMap<String, Object>();
    	HashMap<String, Object> jres = new HashMap<String, Object>();
    	String jmsg = null;
    	String jreply = null;

		log.log(LogLevel.INFO, "ReplyIPCProducer: produce");

    	jreq.put(CMD, "produce");
    	jreq.put(TOPIC, this.producerTopic);
    	jreq.put(CLIENTID, this.clientID);
    	jreq.put(DATA, data);

    	jmsg = map2json(jreq);
    	if (jmsg == null)
    		return false;
    	
    	jreply = send2broker(this.brokerHost, this.brokerPort, jmsg);
    	if (jreply == null)
    		return false;
    	
    	jres = json2map(jreply);
    	if (jres == null)
    		return false;
 
    	Long return_code = (Long) jres.get(REPLY);
		if (return_code.intValue() != 200)
    		return false;

    	return true;   
    }
}