package replyipc;

import java.util.HashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.aicas.jamaica.car.system.logging.LogLevel;

import replyipc.ReplyIPCBase;


public class ReplyIPCConsumer extends ReplyIPCBase { 
    private final int maxConsumMilisecondTimeout = 1000*20;

    /**
     * Constructor
     * @param  brokerHost      broker message server host
     * @param  brokerPort      broker message server port
     * @param  producerTopic   topic name
     */
    public ReplyIPCConsumer(String brokerHost, Integer brokerPort, String producerTopic) {
        super(brokerHost, brokerPort, producerTopic);
    }

    /**
     * do a registration
     * @return true or false
     */
    public boolean register() {
		log.log(LogLevel.INFO, "ReplyIPCConsumer: register");
        return registerOrleave(brokerHost, brokerPort, producerTopic, false, true);
    }

    /**
     * do a unregistration
     * @return true or false
     */
    public boolean leave() {
		log.log(LogLevel.INFO, "ReplyIPCConsumer: leave");
    	return registerOrleave(brokerHost, brokerPort, producerTopic, false, false);
    }

    /**
     * consume data from server
     * @return true or false
     */
    public Object consume() {
        HashMap<String, Object> jreq = new HashMap<String, Object>();
        HashMap<String, Object> jres = new HashMap<String, Object>();        
        String jmsg = null;
        String jreply = null;

		log.log(LogLevel.INFO, "ReplyIPCConsumer: consume");

        jreq.put(CMD, "consume");
        jreq.put(CLIENTID, this.clientID);

        jmsg = map2json(jreq);
        if (jmsg == null)
               return false;

        jreply = send2broker(this.brokerHost, this.brokerPort, jmsg);
        if (jreply == null)
               return false;
        
        jres = json2map(jreply);
        if (jres == null)
               return false;
 
        Long return_code = (Long) jres.get(REPLY);
        if (return_code.intValue() != 200)
               return false;

        return jres;
    }

    /**
     * CallAble class for running task through ExecutorService
     */
    class ConsumeCall implements Callable<Object> {
        @Override
        public Object call() throws Exception { 
            boolean stopFlag = false;
            Object res = null;

    		log.log(LogLevel.INFO, "ReplyIPCConsumer: call");

            try {
                while (!stopFlag) {
                    res = consume(maxConsumMilisecondTimeout);
                    if (res.getClass().isInstance(boolean.class)) {
                    	boolean bres = (Boolean)res;
                    	if (!bres)
                    		return null;
                    }
                    else {
                        stopFlag = true;
                    }
                }
            } catch (Exception e) {
                return null;
            }
            return res;
        }
    }

    /**
     * consume data from server. override consume method and support ExecutorService way
     * @param  callable   shall be true.
     * @return java Object - server reply or null
     */
    public Object consume(boolean callable) {
		log.log(LogLevel.INFO, "ReplyIPCConsumer: consume");

    	ConsumeCall cc = new ConsumeCall();
        FutureTask<Object> task = new FutureTask<Object>(cc);
        ExecutorService exec = Executors.newSingleThreadExecutor();
        Object ts = exec.submit(task);
        try {
            ts = task.get(maxConsumMilisecondTimeout, TimeUnit.MILLISECONDS);
        }
        catch(ExecutionException e){
            return null;
        }
        catch (InterruptedException e){
            return null;            
        } 
        catch (TimeoutException e) {
            return null;
        }
        return ts;
    }

    /**
     * consume data from server. override consume method and support getting data with timeout on server side
     * @param  timeout   timeout in milliseconds
     * @return java Object - server reply or null
     */
    public Object consume(int timeout) {
        HashMap<String, Object> jreq = new HashMap<String, Object>();
        HashMap<String, Object> jres = new HashMap<String, Object>();        
        String jmsg = null;
        String jreply = null;

        log.log(LogLevel.INFO, "ReplyIPCConsumer: consume");

        jreq.put(CMD, "consume");
        jreq.put(TOPIC, this.producerTopic);  //no need anymore
        jreq.put(CLIENTID, this.clientID);
        jreq.put("waitUntil", timeout);

        jmsg = map2json(jreq);
        if (jmsg == null)
               return false;

        jreply = send2broker(this.brokerHost, this.brokerPort, jmsg);
        if (jreply == null)
               return false;
        
        jres = json2map(jreply);
        if (jres == null)
               return false;
 
        Long return_code = (Long) jres.get(REPLY);
        if (return_code.intValue() != 200)
               return false;

        return jres;
    }
}