package carlogger;

import com.aicas.jamaica.car.system.CAR;
import com.aicas.jamaica.car.system.logging.LogLevel;
import com.aicas.jamaica.car.system.logging.Logger;

public class LoggerManager {
	private static boolean useRealLogger = true;
	public static void setUseRealLogger(boolean r) {
		useRealLogger = r;
	}

	public static Logger getLogger(Class<?> c) {
		if (useRealLogger)
			return CAR.getLoggerManager().getLogger(c);
		else
			return new Logger( ) {
	
				@Override
				public boolean isEnabled(LogLevel arg0) {
					return true;
				}
	
				@Override
				public void log(LogLevel arg0, String arg1) {
					System.out.println(arg1);
				}
	
				@Override
				public void log(LogLevel arg0, Throwable arg1) {
					arg1.printStackTrace(System.out);
				}
	
				@Override
				public void log(LogLevel arg0, String arg1, Throwable arg2) {
					System.out.println(arg1);
					arg2.printStackTrace(System.out);
				}
			};
	}
}

