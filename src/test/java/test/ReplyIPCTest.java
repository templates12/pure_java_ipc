package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.After;
import org.junit.Before;
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
import org.junit.Test;
//import org.junit.jupiter.api.Disabled;


import com.aicas.jamaica.car.system.logging.LogLevel;
import com.aicas.jamaica.car.system.logging.Logger;

import replyipc.ReplyIPCConsumer;
import replyipc.ReplyIPCProducer;
import replyipc.ReplyIPCServer;

import carlogger.LoggerManager;

public class ReplyIPCTest {

	static {
		LoggerManager.setUseRealLogger(false);
	}
	static Logger log = LoggerManager.getLogger(ReplyIPCTest.class);


	//@BeforeAll
    //@Before
    public ReplyIPCServer setUp() throws Exception {         
        Logger log = LoggerManager.getLogger(ReplyIPCTest.class);
    	ReplyIPCServer server = null;
        log.log(LogLevel.INFO, "start");
        int port = 10100 + (int)(Math.random() * 1000);
        server = new ReplyIPCServer(port, 10, "");
        new Thread(server).start();
        Thread.sleep(100);
        return server;
    }
    
    //@AfterAll
    //@After
    public void tearDown(ReplyIPCServer server) throws Exception { 
        if (!(server == null)) {
            server.stopRequired = true;
            Thread.sleep(100);
        }
        log.log(LogLevel.INFO, "stop");
    }

    //@Disabled
    @Test
    public void test_producer_register_unregister() throws Exception {
        ReplyIPCServer server = setUp();
        ReplyIPCProducer producer = new ReplyIPCProducer("", server.getPort(), this.toString());
        boolean res = producer.register();
        assertEquals(true, res);
        res = producer.leave();
        assertEquals(true, res);
        tearDown(server);
        return;
    }

    //@Disabled
    @Test
    public void test_producer_produce() throws Exception {
        ReplyIPCServer server = setUp();
        ReplyIPCProducer producer = new ReplyIPCProducer("", server.getPort(), this.toString());
        boolean res = producer.register();
        assertEquals(true, res);
        
        String msg = "{\"properties\": [{\"propname\": \"a\", \"propval\": 10},\n" + 
                "                             {\"propname\": \"a\", \"propval\": 10}], \n" + 
                "               \"body\": \"EVENT_RADIO_TURN_OFF\" }";
        producer.produce(msg);
        res = producer.leave();
        assertEquals(true, res);
        tearDown(server);
        return;
    }

    //@Disabled
    @Test
    public void test_consumer_register_unregister() throws Exception {
        ReplyIPCServer server = setUp();
        ReplyIPCProducer producer = new ReplyIPCProducer("", server.getPort(), this.toString());
        boolean res = producer.register();
        assertEquals(true, res);
        
        ReplyIPCConsumer consumer = new ReplyIPCConsumer("", server.getPort(), this.toString());
        res = consumer.register();
        assertEquals(true, res);
        res =  consumer.leave();
        assertEquals(true, res);

        res = producer.leave();
        assertEquals(true, res);
        tearDown(server);
        return;
    }

    @Test
    public void test_producer_produce_consumer_later() throws Exception {
        ReplyIPCServer server = setUp();
        ReplyIPCProducer producer = new ReplyIPCProducer("", server.getPort(), this.toString());
        boolean res = producer.register();
        assertEquals(true, res);
        
        String msg = "{\"properties\": [{\"propname\": \"a\", \"propval\": 10},\n" + 
                "                             {\"propname\": \"a\", \"propval\": 10}], \n" + 
                "               \"body\": \"EVENT_RADIO_TURN_OFF\" }";
        producer.produce(msg);
        Thread.sleep(1000);

        producer.produce(msg);
        Thread.sleep(1000);

        ReplyIPCConsumer consumer = new ReplyIPCConsumer("", server.getPort(), this.toString());
        res = consumer.register();
        assertEquals(true, res);
        
        Object resData = consumer.consume();
        if (resData.getClass().isInstance(boolean.class) && (boolean)resData == false)
            assertEquals(false, (boolean)resData);
        log.log(LogLevel.INFO, "##########  CONSUMER RES =" + resData);

        res = producer.leave();
        assertEquals(true, res);
        tearDown(server);
        return;
    }
    
    //@Disabled
    @Test
    public void test_consumer_consume() throws Exception {
        ReplyIPCServer server = setUp();
        ReplyIPCProducer producer = new ReplyIPCProducer("", server.getPort(), this.toString());
        boolean res = producer.register();
        assertEquals(true, res);
        
        ReplyIPCConsumer consumer = new ReplyIPCConsumer("", server.getPort(), this.toString());
        res = consumer.register();
        assertEquals(true, res);
        
        Object resData = consumer.consume();
        if (resData.getClass().isInstance(boolean.class) && (boolean)resData == false)
            assertEquals(false, (boolean)resData);
        log.log(LogLevel.INFO, "##########  CONSUMER RES =" + resData);
        Thread.sleep(1000);
        

        String msg = "{\"properties\": [{\"propname\": \"a\", \"propval\": 10},\n" + 
                "                             {\"propname\": \"a\", \"propval\": 10}], \n" + 
                "               \"body\": \"EVENT_RADIO_TURN_OFF\" }";
        producer.produce(msg);
        Thread.sleep(2*1000);

        resData = consumer.consume();
        log.log(LogLevel.INFO, "##########  CONSUMER RES =" + resData);
        if (resData.getClass().isInstance(boolean.class) && (boolean)resData == false)
            assertEquals(true, (boolean)resData);
        else
            assertNotEquals(null, resData);

        // --------------------------------------
        msg = "{\"properties\": [{\"propname\": \"a\", \"propval\": 10},\n" + 
                "                             {\"propname\": \"a\", \"propval\": 10}], \n" + 
                "               \"body\": \"EVENT_RADIO_TURN_ON\" }";
        producer.produce(msg);
        Thread.sleep(2*1000);

        resData = consumer.consume();
        log.log(LogLevel.INFO, "##########  CONSUMER RES =" + resData);
        if (resData.getClass().isInstance(boolean.class) && (boolean)resData == false)
            assertEquals(true, (boolean)resData);
        else
            assertNotEquals(null, resData);

        res = consumer.leave();
        assertEquals(true, res);
        
        res = producer.leave();
        assertEquals(true, res);
        tearDown(server);
        return;
    }

    @Test
    public void test_consumer_consume_wait() throws Exception {
        ReplyIPCServer server = setUp();
        ReplyIPCProducer producer = new ReplyIPCProducer("", server.getPort(), this.toString());
        boolean res = producer.register();
        assertEquals(true, res);
        
        // --------------------------------------
        String msg = "{\"properties\": [{\"propname\": \"a\", \"propval\": 10},\n" + 
                "                             {\"propname\": \"a\", \"propval\": 10}], \n" + 
                "               \"body\": \"EVENT_RADIO_TURN_OFF\" }";

        ReplyIPCConsumer consumer = new ReplyIPCConsumer("", server.getPort(), this.toString());
        res = consumer.register();
        assertEquals(true, res);

        new Thread("ProiducerWait") {
            public void run() {
                try {
                    Thread.sleep(1000*10);
                    producer.produce(msg);
                    producer.produce(msg);
                    Thread.sleep(1000*1);
                }
                catch (InterruptedException e){
                    return;
                }
            }
        }.start();

        Object resData = consumer.consume(1000*30);
        log.log(LogLevel.INFO, "##########  CONSUMER RES =" + resData);        
        assertNotEquals(null, resData);

        resData = consumer.consume(1000*30);
        log.log(LogLevel.INFO, "##########  CONSUMER RES =" + resData);        
        assertNotEquals(null, resData);

        producer.produce("TEST_WAIT_UNTIL");
        producer.produce("TEST_WAIT_UNTIL1");
        Thread.sleep(1000*1);
        producer.produce("TEST_WAIT_UNTIL2");
        Thread.sleep(1000*1);


        resData = consumer.consume(1000);
        log.log(LogLevel.INFO, "##########  CONSUMER RES =" + resData);
        assertNotEquals(null, resData);

        resData = consumer.consume(1000);
        log.log(LogLevel.INFO, "##########  CONSUMER RES =" + resData);
        assertNotEquals(null, resData);
        
        res = consumer.leave();
        assertEquals(true, res);
        
        res = producer.leave();
        assertEquals(true, res);

        tearDown(server);
        return;
    }
    //@Disabled
    @Test
    public void test_consumer_consume_callable() throws Exception {
        // --------------------------------------
        String msg = this.toString() + "_" + (int)(Math.random() * 1000);
        ReplyIPCServer server = setUp();
        ReplyIPCProducer producer = new ReplyIPCProducer("", server.getPort(), this.toString());
        boolean res = producer.register();
        assertEquals(true, res);

        ReplyIPCConsumer consumer = new ReplyIPCConsumer("", server.getPort(), this.toString());
        res = consumer.register();
        assertEquals(true, res);

        new Thread("ProiducerWait") {
            public void run() {
                try {
                    Thread.sleep(1000*10);
                    producer.produce(msg);
                    producer.produce(msg);
                    Thread.sleep(1000*10);
                    producer.produce(msg);
                    producer.produce(msg);
                }
                catch (InterruptedException e){
                    return;
                }
            }
        }.start();

        Object resData = consumer.consume(true);
        log.log(LogLevel.INFO, "##########  CONSUMER RES =" + resData);        
        assertNotEquals(null, resData);

        resData = consumer.consume(true);
        log.log(LogLevel.INFO, "##########  CONSUMER RES =" + resData);        
        assertNotEquals(null, resData);

        producer.produce("TEST_WAIT_UNTIL");
        producer.produce("TEST_WAIT_UNTIL1");
        Thread.sleep(1000*1);

        resData = consumer.consume(true);
        log.log(LogLevel.INFO, "##########  CONSUMER RES =" + resData);
        assertNotEquals(null, resData);
        
        res = consumer.leave();
        assertEquals(true, res);
        
        res = producer.leave();
        assertEquals(true, res);

        tearDown(server);
        return;
    }
}
